const express = require("express");
const app = express();
const port = process.env.PORT || 3000;
const {encryptText, decryptText} = require("./util");
const bodyParser = require('body-parser');


app.use(bodyParser.urlencoded({ extended: false }));

app.post('/encrypt', (req, res) => {
    let key = req.body.key;
    let text = req.body.text;
    encryptText(key, text).then((result) => {
        res.send({msg: "request succeeded",response: result})
    });
});

app.post('/decrypt', (req, res) => {
    let key = req.body.key;
    let cipherText = req.body.text;
    decryptText(key, cipherText).then((result) => {
        res.send({msg: "request succeeded", response: result})
    })
});


app.listen(port, () => {
    console.log("Express server has started listening on port --> ", port);
});