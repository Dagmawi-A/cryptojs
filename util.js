const CryptoJS = require("crypto-js");


function encryptText(key, text) {
    let encryptedText = CryptoJS.AES.encrypt(text, key).toString();
    return new Promise(resolve => {
        resolve(encryptedText)
    })
}


function decryptText(key, cipherText) {
    let bytes = CryptoJS.AES.decrypt(cipherText, key);
    let decryptedText = bytes.toString(CryptoJS.enc.Utf8);
    return new Promise(resolve => {
        resolve(decryptedText)
    })

}


module.exports = {
    encryptText,
    decryptText
};